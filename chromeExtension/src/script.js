var defaultUrl = 'megogo.net';
var prefixUrl = $('#env-box').val(); // todo on load

$('#env-box').change(function (e) {
    prefixUrl = $(this).val();
});

$('a').click(function (e) {
    e.preventDefault();
    var data = {
        url: "http://" + (prefixUrl ? prefixUrl + "." : '') + defaultUrl + $(this).attr('href')
    };

    if ($(this).data('selected') == false) {
        data.selected = false;
    }

    chrome.tabs.create(data, function (tab) {
        var tabId = tab.id;
        chrome.tabs.onUpdated.addListener(function(tabId, changeInfo) {
            if (changeInfo.status == "complete") {
                alert(tabId);
                chrome.tabs.remove(tabId);
                chrome.tabs.onUpdated.removeListene(arguments.callee);
            }
        });
    });
});
