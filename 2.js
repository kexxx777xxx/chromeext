function translate(text) {
    var ohAll = [];
    var langs = ["ab", "av", "ae", "az", "ay", "ak", "sq", "am", "en", "ar", "am", "as", "aa", "af", "ak", "bm", "eu", "ba", "be", "bn", "my", "bi", "bg", "bs", "br", "cy", "hu", "ve", "vo", "wo", "vi", "gl", "lg", "hz", "kl", "el", "ka", "gn", "gu", "gd", "dr", "da", "dz", "dv", "zu", "he", "ig", "yi", "id", "ia", "ie", "iu", "ik", "ga", "is", "es", "it", "yo", "kk", "kn", "kr", "ca", "ks", "qu", "ki", "kj", "ky", "zh", "kv", "kg", "ko", "kw", "co", "xh", "ku", "km", "lo", "la", "lv", "ln", "lt", "lu", "lb", "mk", "mg", "ms", "ml", "mt", "mi", "mr", "mh", "mo", "mn", "gv", "nv", "na", "nd", "nr", "ng", "de", "ne", "nl", "no", "ny", "nn", "oj", "oc", "or", "om", "os", "pi", "pa", "fa", "pl", "pt", "ps", "rm", "rw", "ro", "rn", "ru", "sm", "sg", "sa", "sc", "ss", "sr", "si", "sd", "sk", "sl", "so", "st", "sw", "su", "tl", "tg", "th", "ty", "ta", "tt", "tw", "te", "bo", "ti", "to", "tn", "ts", "tr", "tu", "uz", "ug", "uk", "ur", "fo", "fj", "fl", "fi", "fr", "fy", "ff", "ha", "hi", "ho", "hr", "cu", "ch", "ce", "cs", "za", "cv", "sv", "sn", "ee", "eo", "et", "jv", "ja"]
    var count = 0;
    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function () {
        var json = JSON.parse(xmlhttp.responseText)
        var data = json.sentences[0].trans;
        if (ohAll.indexOf(data) === -1) {
            ohAll.push(data);
        }
        if (++count == langs.length) {
            console.log(ohAll.join(","));
        }
    }

    for (var i in langs) {
        var lang = langs[i];
        var url = "https://translate.google.com/translate_a/t?client=te&text=" + text + "&hl=ru&sl=ru&tl=" + lang + "&multires=1&otf=1&ssel=0&tsel=0&uptl=ru&sc=1";
        xmlhttp.open("GET", url, false);
        xmlhttp.send();
    }
}